package com.ellisg.demobackend.dao;
import com.ellisg.demobackend.model.Person;
import java.util.UUID;
import java.util.List;

public interface PersonDAO {
    int insertPerson(UUID id, Person person );
    default int insertPerson(Person person) {
        UUID id = UUID.randomUUID();
        return insertPerson(id, person);
    }

    List<Person> selectAllPeople();
}
